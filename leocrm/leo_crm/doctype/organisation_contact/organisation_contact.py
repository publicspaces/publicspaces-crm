# Copyright (c) 2022, PublicSpaces.net and contributors
# For license information, please see license.txt
from __future__ import unicode_literals
import json
import frappe
from frappe.model.document import Document


class OrganisationContact(Document):

	def db_update(self):
		d = self.get_valid_dict(convert_dates_to_str=True)
		if frappe.db.exists("Person Role", d.name):
			doc = frappe.get_doc('Person Role', d.name)
			doc.person = d.person
			doc.role = d.role
			doc.save();
		else:
			new = frappe.get_doc({
			    'doctype': 'Person Role',
			    'organisation': d.parent,
			    'role': d.role,
			    'parent': d.person,
			    'parenttype':'Person',
			    'parentfield':'organisations',

			})
			new.insert()

		person = frappe.get_doc("Person", d.person)
		person.notify_update()
		#doc.insert()
		# with open("data_file.json", "w+") as read_file:
		# 	json.dump(d, read_file)

