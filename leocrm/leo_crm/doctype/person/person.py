# Copyright (c) 2022, PublicSpaces.net and contributors
# For license information, please see license.txt

# import frappe
from frappe.model.document import Document

class Person(Document):
	def before_insert(self):
		self.fullname = self.firstname + (" " + self.middlename if self.middlename else "") + " " + self.lastname
	def before_save(self):
		self.fullname = self.firstname + (" " + self.middlename if self.middlename else "") + " " + self.lastname
