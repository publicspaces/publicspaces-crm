// Copyright (c) 2022, PublicSpaces.net and contributors
// For license information, please see license.txt

async function leo_add_org_to_conversation(frm, org) {

	// if the org is not yet assigned to this doc 
	if (!frm.doc.organisations || !frm.doc.organisations.find(o => o.organisation === org)) {
		// load org data and add to document
		let fullorg = await frappe.db.get_doc("Organisation",org);
		frappe.utils.add_link_title("Organisation", org, fullorg.org_name)
		let row = frm.add_child('organisations', {
		    organisation: org,
		});
		frm.refresh_field('organisations');
	}
}

async function leo_add_org_intersection_of_projects_and_participants(frm) {
	if (!frm.doc.projects) return;
	if (!frm.doc.participants) return; 
	for (let project of frm.doc.projects) {
		// check if org
		let pd = await frappe.db.get_doc("Project",project.project)
		// get all praticipants
		let participant_organisations = await frappe.db.get_list("Person Role",{
			parent_doctype: 'Person',
			fields: ['organisation'],
			filters: [[ 'parent','IN', frm.doc.participants.map(p=>p.participant) ]]
		})
		// get intersection of participant roles and project organisation
		let orgs = participant_organisations.map(po=>po.organisation).filter(io=> pd.organisations.map(pi=>pi.organisation).includes(io));
		for (let org of orgs) {
			await leo_add_org_to_conversation(frm, org)
		}
	}

}

frappe.ui.form.on('Conversation Log', {
	refresh: function(frm) {
		if (frm.is_new()) frm.set_value("date", frappe.datetime.now_datetime());

		frappe.db.get_value('Person', {email: frappe.session.user_email}, ['name','email','fullname'])
	    .then(r => {
	        let values = r.message;

            if (values.email && frm.is_new()) {
            	frappe.utils.add_link_title("Person",values.name,values.fullname)
				let row = frm.add_child('participants', {
				    participant: values.name,
				});

				frm.refresh_field('participants');
            }

	    })
	},
	projects: async function(frm) {
		await leo_add_org_intersection_of_projects_and_participants(frm);
	},
	participants: async function(frm) {
		await leo_add_org_intersection_of_projects_and_participants(frm);
	},
	partnerships: async function(frm) {

		for (let partnership of frm.doc.partnerships) {
			// load partnership
			let pd = await frappe.db.get_doc("Partnership",partnership.partnership)
			// if partnership doc has org assigned
			if (pd.organisation) {
				await leo_add_org_to_conversation(frm, pd.organisation)
			}
		}
	}
});
