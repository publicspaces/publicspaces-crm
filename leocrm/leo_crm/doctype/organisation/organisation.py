# Copyright (c) 2022, PublicSpaces.net and contributors
# For license information, please see license.txt

import frappe
import json
from frappe.model.document import Document


def map_names(p):
	return p.name

class Organisation(Document):

	def db_update(self):

		# Delete Person Role's that are removed from the organisation.
		ps = frappe.db.get_all('Person Role',

		    filters=[
		        ['organisation','=',self.name],
		        ['parenttype','=', 'Person'],
		        ['parentfield','=', 'organisations'],
		        ['name', 'NOT IN', list(map(map_names,self.contacts))]
		    ],
		    fields=['name']
		)
		frappe.db.delete("Person Role", ("IN", list(map(map_names, ps))))
		return super().db_update()

	def load_from_db(self):
		data = super().load_from_db()

		children = (
			frappe.db.get_values(
				'Person Role',
				{"organisation": self.name, "parenttype": "Person", "parentfield": 'organisations'},
				"*",
				as_dict=True,
				order_by="idx asc",
			)
			or []
		)
		# swap links
		for p in children:
			p.person = p.parent
		self.set('contacts', children)
		# sometimes __setup__ can depend on child values, hence calling again at the end
		if hasattr(self, "__setup__"):
			self.__setup__()

		return data

