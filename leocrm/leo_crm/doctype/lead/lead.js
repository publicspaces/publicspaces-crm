// Copyright (c) 2022, PublicSpaces.net and contributors
// For license information, please see license.txt

frappe.ui.form.on('Lead', {
    refresh: function(frm) {

		frm.add_custom_button(`${__("Call")}`, function(){
					window.location.href = "tel:"+$('input[data-fieldname="phone"]').val();
		});
		frm.add_custom_button(`${__("Send mail")}`, function(){
					window.location.href = "mailto:"+$('input[data-fieldname="email"]').val();
		});

		const $phone =  $('div[data-fieldname="phone"]')
		$phone.append(
			`<span class="custom-btn phone-btn">
				<a class="btn-open no-decoration" title="${__("Call")}" >
					${frappe.utils.icon('call', 'sm')}
				</a>
			</span>`
		);

		const $link_open = $phone.find('.phone-btn');

		$link_open.on('click',() =>{
			window.location.href = 'tel:'+$('input[data-fieldname="phone"]').val();
		})

		const $email =  $('div[data-fieldname="email"]')
		$email.append(
			`<span class="custom-btn email-btn">
				<a class="btn-open no-decoration" title="${__("Send mail")}" >
					${frappe.utils.icon('mail', 'sm')}
				</a>
			</span>`
		);

		const $mail_open = $email.find('.email-btn');

		$mail_open.on('click',() =>{
			window.location.href = 'mailto:'+$('input[data-fieldname="email"]').val();
		})

	}
});
