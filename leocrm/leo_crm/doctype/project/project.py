# Copyright (c) 2022, PublicSpaces.net and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document

def map_participants(p):
	return p.person

def map_organisations(p):
	return p.organisation

def map_names(p):
	return p[0]

class Project(Document):
	def before_save(self):

		ps = frappe.db.get_all('Person',
		    filters=[
		        ['name', 'IN', list(map(map_participants, self.people))]
		    ],
		    fields=['fullname'],
		    as_list=True
		)
		self.participants_display = ', '.join(list(map(map_names, ps)));

		ps = frappe.db.get_all('Organisation',
		    filters=[
		        ['name', 'IN', list(map(map_organisations, self.organisations))]
		    ],
		    fields=['org_name'],
		    as_list=True
		)
		self.organisations_display = ', '.join(list(map(map_names, ps)));