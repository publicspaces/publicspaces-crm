

import frappe

@frappe.whitelist()
#@frappe.validate_and_sanitize_search_inputs
def badge_redirect(r):

	if not frappe.has_permission("Organisation", "read"):
		raise frappe.PermissionError

	default = "https://spoelkeuken.publicspaces.net/"

	domain = r
	domainParts = domain.split(".")

	redirect = ""
	if len(domainParts) > 1:
		for p in range(2,1+len(domainParts)):
			search = ".".join(domainParts[-p:])
			orgs = frappe.db.sql(
				"""select o.badge_redirect_url FROM `tabBadge Domain` d
			        LEFT JOIN  `tabOrganisation` o ON (d.parenttype = 'Organisation' AND d.parent = o.name)
			        WHERE d.domain = %(search)s
			""" ,values={'search': search})
			if len(orgs) > 0:
				redirect = orgs[0][0]
				break


	if redirect == "" :
		redirect = default

	return {"redirect":redirect}

