from frappe import _

def get_data():
	return [
		{
			"module_name": "LEO CRM",
			"type": "module",
			"label": _("LEO CRM")
		}
	]
