APP_NAME="leocrm"
variable "FRAPPE_VERSION" {}
variable "ERPNEXT_VERSION" {}
group "default" {
    targets = ["backend", "frontend"]
}
target "backend" {
    dockerfile = "backend.Dockerfile"
    tags = ["toontoet/leocrm-worker:latest"]
    args = {
      "ERPNEXT_VERSION" = ERPNEXT_VERSION
      "FRAPPE_VERSION" = FRAPPE_VERSION
      "APP_NAME" = APP_NAME
    }
}
target "frontend" {
    dockerfile = "frontend.Dockerfile"
    tags = ["toontoet/leocrm-nginx:latest"]
    args = {
      "FRAPPE_VERSION" = FRAPPE_VERSION
      "ERPNEXT_VERSION" = ERPNEXT_VERSION
      "APP_NAME" = APP_NAME
    }
}
