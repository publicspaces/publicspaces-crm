from setuptools import setup, find_packages

with open("requirements.txt") as f:
	install_requires = f.read().strip().split("\n")

# get version from __version__ variable in leocrm/__init__.py
from leocrm import __version__ as version

setup(
	name="leocrm",
	version=version,
	description="Leuk En Overzichtelijk CRM systeem",
	author="PublicSpaces.net",
	author_email="team@publicspaces.net",
	packages=find_packages(),
	zip_safe=False,
	include_package_data=True,
	install_requires=install_requires
)
