## LEO CRM

Leuk En Overzichtelijk CRM systeem

### Build Docker images

To build docker images using this custom app use:
```
ERPNEXT_VERSION=v14 FRAPPE_VERSION=v14 docker buildx bake
```


#### License

MIT
